/*
 *  Copyright (C) 2019 Bc. Pavel Stambrecht - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 *  Written by Pavel Stambrecht
 * /
 */

package cz.stambrecht.notes

import cz.stambrecht.notes.io.rest.model.Note
import org.junit.Test

/**
 * Test connected with model class [Note]
 *
 * @author Pavel Stambrecht
 * @date 24/03/2019
 */
class NoteUnitTest {

    @Test
    fun note_isRemovable() {
        val note = Note(title = "Test title")
        assert(!note.isRemovable())
        note.id = 1L
        assert(note.isRemovable())
    }
}