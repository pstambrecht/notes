/*
 *  Copyright (C) 2019 Bc. Pavel Stambrecht - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 *  Written by Pavel Stambrecht
 * /
 */

package cz.stambrecht.notes.utils

import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

/**
 * [Asyncer] executes [Runnable] tasks on background thread.
 *
 * @author Pavel Stambrecht
 * @date 24/03/2019
 */
object Asyncer {

    private const val CORE_POOL_SIZE = 2
    private const val MAXIMUM_POOL_SIZE = 10
    private const val KEEP_ALIVE_TIME_MS = 2000L

    private val executor = ThreadPoolExecutor(
        CORE_POOL_SIZE,
        MAXIMUM_POOL_SIZE,
        KEEP_ALIVE_TIME_MS,
        TimeUnit.SECONDS,
        LinkedBlockingQueue<Runnable>()
    )

    /**
     * Processes [Runnable] on [ThreadPoolExecutor]
     * @param runnable [Runnable] instance to run on executor
     */
    fun process(runnable: Runnable) {
        executor.execute(runnable)
    }

    /**
     * Processes [Runnable] on [ThreadPoolExecutor]
     * @param runnable [Runnable] instance to run on executor
     */
    fun process(runnable: () -> Unit) {
        executor.execute(runnable)
    }

}