/*
 *  Copyright (C) 2019 Bc. Pavel Stambrecht - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 *  Written by Pavel Stambrecht
 * /
 */

package cz.stambrecht.notes.fragment

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import cz.stambrecht.notes.R
import cz.stambrecht.notes.io.rest.model.Note
import cz.stambrecht.notes.viewmodel.NoteDetailViewModel
import kotlinx.android.synthetic.main.fragment_note_detail.*
import kotlinx.android.synthetic.main.fragment_note_detail.view.*

/**
 * @author Pavel Stambrecht
 * @date 24/03/2019
 */
class NoteDetailFragment : Fragment() {

    companion object {

        private const val EXTRA_NOTE = "extra_note"

        /**
         * Creates new instance of [NoteDetailFragment] with [Note] in arguments
         * under the key [EXTRA_NOTE].
         * @param note Note arguments to add to extras
         */
        //Note is small enough to send it to fragment using parcelable.
        //Change it to start activity only with note id and load data separately.
        fun newInstance(note: Note) = NoteDetailFragment().apply {
            arguments = Bundle().apply {
                putParcelable(EXTRA_NOTE, note)
            }
        }
    }

    private lateinit var noteDetailViewModel: NoteDetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        noteDetailViewModel = ViewModelProviders.of(this).get(NoteDetailViewModel::class.java)
        noteDetailViewModel.init(arguments?.getParcelable(EXTRA_NOTE))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_note_detail, container, false).let {
            it.swipeRefreshLayout.isEnabled = false
            it
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeNote()
        observeError()
        observeIsProcessing()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.note_detail, menu)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        with(activity as AppCompatActivity) {
            setSupportActionBar(toolbar)
            supportActionBar!!.let {
                it.setDisplayHomeAsUpEnabled(true)
                it.setHomeButtonEnabled(true)
                it.title = getString(R.string.title_note)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        noteDetailViewModel.note.removeObservers(this)
        noteDetailViewModel.error.removeObservers(this)
        noteDetailViewModel.isProcessing.removeObservers(this)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean = when (item?.itemId) {
        android.R.id.home -> {
            activity?.onBackPressed()
            true
        }
        R.id.menu_delete -> {
            Snackbar.make(scrollView, "Note has been removed", Snackbar.LENGTH_SHORT)
                .setAction("Undo") { }
                .addCallback(object : Snackbar.Callback() {
                    override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                        super.onDismissed(transientBottomBar, event)
                        if (event != BaseTransientBottomBar.BaseCallback.DISMISS_EVENT_ACTION) {
                            noteDetailViewModel.remove()
                        }
                    }
                }).show()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun observeNote() {
        noteDetailViewModel.note.observe(this, Observer { note ->
            note?.let { textView_note.text = it.title } ?: activity?.onBackPressed()
        })
    }

    private fun observeError() {
        noteDetailViewModel.error.observe(this, Observer {
            Toast.makeText(context, getString(R.string.error_default), Toast.LENGTH_SHORT).show()
        })
    }

    private fun observeIsProcessing() {
        noteDetailViewModel.isProcessing.observe(this, Observer {
            swipeRefreshLayout.isRefreshing = it
        })
    }
}