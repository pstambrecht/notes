/*
 *  Copyright (C) 2019 Bc. Pavel Stambrecht - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 *  Written by Pavel Stambrecht
 * /
 */

package cz.stambrecht.notes.fragment

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import cz.stambrecht.notes.R
import cz.stambrecht.notes.activity.MainActivity
import cz.stambrecht.notes.adapter.NoteAdapter
import cz.stambrecht.notes.adapter.common.DiffAdapter
import cz.stambrecht.notes.extension.hideSoftwareKeyboard
import cz.stambrecht.notes.io.rest.model.Note
import cz.stambrecht.notes.viewmodel.NotesViewModel
import kotlinx.android.synthetic.main.fragment_notes.*
import kotlinx.android.synthetic.main.fragment_notes.view.*

/**
 * [NotesFragment] represents screen with list of notes. It also provide functions to
 * create new [Note] and open [Note] detail. This class is using [NotesViewModel]
 *
 * @author Pavel Stambrecht
 * @date 18/03/2019
 */
class NotesFragment : Fragment() {

    private lateinit var notesViewModel: NotesViewModel
    private lateinit var bottomSheetBehaviour: BottomSheetBehavior<LinearLayout>
    private var mainActivity: MainActivity? = null

    private val onSwipe = SwipeRefreshLayout.OnRefreshListener {
        notesViewModel.loadNotes()
    }

    private val onItemClickListener = object : DiffAdapter.OnItemClickListener<Note> {
        override fun onItemClicked(item: Note, type: Int) {
            mainActivity?.changeContentFragment(NoteDetailFragment.newInstance(item), true)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is MainActivity) {
            mainActivity = context
        } else {
            throw IllegalStateException("Notes fragment have to be attached to ${MainActivity::class.java.name}")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mainActivity = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        notesViewModel = ViewModelProviders.of(this).get(NotesViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_notes, container, false).let { view ->

            //bottom sheet setup
            bottomSheetBehaviour = BottomSheetBehavior.from(view.linearLayout_bottomSheet)
            bottomSheetBehaviour.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
                override fun onSlide(p0: View, p1: Float) {
                    //empty
                }

                override fun onStateChanged(p0: View, p1: Int) {
                    //BottomBehaviour has a strange behaviour. We need to invalidate view.
                    view.linearLayout_bottomSheet.invalidate()
                }
            })

            //recycler view setup
            with(view.recyclerView) {
                layoutManager = LinearLayoutManager(context)
                addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
                adapter = NoteAdapter().apply {
                    onItemClickListener = this@NotesFragment.onItemClickListener
                }
            }

            //button setup - setting drawable in XML is not working, we have to set it up programmatically
            with(view.button_add) {
                setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_add, 0, 0, 0)
                compoundDrawablePadding = resources.getDimensionPixelSize(R.dimen.margin_16dp)
                setOnClickListener {
                    if (bottomSheetBehaviour.state == BottomSheetBehavior.STATE_EXPANDED) {
                        bottomSheetBehaviour.state = BottomSheetBehavior.STATE_COLLAPSED
                    } else {
                        view.button_save.isEnabled = false
                        view.editText_title.let {
                            it.text.clear()
                            it.requestFocus()
                        }
                        bottomSheetBehaviour.state = BottomSheetBehavior.STATE_EXPANDED
                    }
                }
            }

            //Title edit text setup
            view.editText_title.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    view.button_save.isEnabled = s?.length ?: 0 > 0
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                    //empty
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    //empty
                }

            })

            //button save
            view.button_save.setOnClickListener {
                notesViewModel.createNote(Note(title = view.editText_title.text.toString()))
                bottomSheetBehaviour.state = BottomSheetBehavior.STATE_COLLAPSED
                activity?.hideSoftwareKeyboard()
            }

            view.swipeRefreshLayout.setOnRefreshListener(onSwipe)
            view
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeNotes()
        observeIsLoading()
        observeError()
    }

    override fun onStart() {
        super.onStart()
        notesViewModel.loadNotes()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        swipeRefreshLayout.setOnRefreshListener(null)
        notesViewModel.notes.removeObservers(this)
        notesViewModel.isLoading.removeObservers(this)
        notesViewModel.error.removeObservers(this)
    }

    private fun observeNotes() {
        notesViewModel.notes.observe(this, Observer {
            (recyclerView.adapter as NoteAdapter).submitList(it)
        })
    }

    private fun observeIsLoading() {
        notesViewModel.isLoading.observe(this, Observer {
            swipeRefreshLayout.isRefreshing = it
            button_add.isEnabled = !it
        })
    }

    private fun observeError() {
        notesViewModel.error.observe(this, Observer {
            Toast.makeText(context, getString(R.string.error_default), Toast.LENGTH_SHORT).show()
        })
    }
}