/*
 *  Copyright (C) 2019 Bc. Pavel Stambrecht - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 *  Written by Pavel Stambrecht
 * /
 */

package cz.stambrecht.notes.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import cz.stambrecht.notes.BuildConfig
import cz.stambrecht.notes.R
import kotlinx.android.synthetic.main.fragment_about.view.*

/**
 * @author Pavel Stambrecht
 * @date 19/03/2019
 */
class AboutFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_about, container, false).let {
            it.textView_about.text = getString(R.string.about, BuildConfig.VERSION_NAME)
            it
        }
}