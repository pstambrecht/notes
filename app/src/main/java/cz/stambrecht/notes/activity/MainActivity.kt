/*
 *  Copyright (C) 2019 Bc. Pavel Stambrecht - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 *  Written by Pavel Stambrecht
 * /
 */

package cz.stambrecht.notes.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import cz.stambrecht.notes.R
import cz.stambrecht.notes.fragment.AboutFragment
import cz.stambrecht.notes.fragment.NotesFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener {
        when (it.itemId) {
            R.id.menu_notes -> {
                changeContentFragment(NotesFragment(), clearBackStack = true)
                true
            }
            R.id.menu_about -> {
                changeContentFragment(AboutFragment(), clearBackStack = true)
                true
            }
            else -> throw IllegalStateException("Navigation menu not supported")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        changeContentFragment(NotesFragment())
        bottomNavigationView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
    }

    override fun onDestroy() {
        super.onDestroy()
        bottomNavigationView.setOnNavigationItemSelectedListener(null)
    }

    /**
     * Changes content fragment
     * @param fragment fragment to inflate
     * @param addToBackStack Option to add transaction to back stack. True is add. Default is false.
     * @param clearBackStack Option to clear back stack. True to clear. Default is false.
     */
    fun changeContentFragment(fragment: Fragment, addToBackStack: Boolean = false, clearBackStack: Boolean = true) {
        if (clearBackStack) {
            supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        }

        supportFragmentManager.beginTransaction()
            .replace(R.id.frameLayout_container, fragment).let {
                if (addToBackStack) {
                    it.addToBackStack(null)
                }
                it.commit()
            }
    }
}
