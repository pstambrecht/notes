/*
 *  Copyright (C) 2019 Bc. Pavel Stambrecht - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 *  Written by Pavel Stambrecht
 * /
 */

package cz.stambrecht.notes.io.rest.interceptor

import okhttp3.Interceptor
import okhttp3.Response

/**
 * [ApiaryHeaderInterceptor] adds headers to request to Apiary API
 *
 * @author Pavel Stambrecht
 * @date 23/03/2019
 */
class ApiaryHeaderInterceptor : Interceptor {

    companion object {
        private const val HEADER_CONTENT_TYPE = "Content-Type"
        private const val APPLICATION_JSON = "application/json"
    }

    override fun intercept(chain: Interceptor.Chain): Response = chain.proceed(
        chain.request()
            .newBuilder()
            .header(HEADER_CONTENT_TYPE, APPLICATION_JSON)
            .build()
    )


}