/*
 *  Copyright (C) 2019 Bc. Pavel Stambrecht - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 *  Written by Pavel Stambrecht
 * /
 */

package cz.stambrecht.notes.io.rest.exception

/**
 * @author Pavel Stambrecht
 * @date 23/03/2019
 */
class UnsuccessResponseException : RuntimeException()