/*
 *  Copyright (C) 2019 Bc. Pavel Stambrecht - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 *  Written by Pavel Stambrecht
 * /
 */

package cz.stambrecht.notes.io.rest.api

import cz.stambrecht.notes.io.rest.model.Note
import retrofit2.Call
import retrofit2.http.*

/**
 * [ApiaryApi] interface defines an interface of Apiary
 * notes api.
 *
 * @author Pavel Stambrecht
 * @date 18/03/2019
 */
interface ApiaryApi {

    /**
     * Method to load list of all notes from API
     * @return Retrofit's [Call]
     *
     * @see Note
     */
    @GET("notes")
    fun getNotes(): Call<List<Note>>

    /**
     * Method to load one note by id from API
     * @param id Note's id
     * @return Retrofit's [Call]
     *
     * @see Note
     */
    @GET("notes/{id}")
    fun getNote(@Path("id") id: Long): Call<Note>

    /**
     * Method to create new note on API
     * @param note Note to create on backend
     * @return Retrofit's [Call]
     *
     * @see Note
     */
    @POST("notes")
    fun createNote(@Body note: Note): Call<Note>

    /**
     * Method to remove existing note on API
     * @param id Id of note to remove on backend
     * @return Retrofit's [Call]
     *
     * @see Note
     */
    @DELETE("notes/{id}")
    fun removeNote(@Path("id") id: Long): Call<Any>
}