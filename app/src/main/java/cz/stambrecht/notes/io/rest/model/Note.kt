/*
 *  Copyright (C) 2019 Bc. Pavel Stambrecht - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 *  Written by Pavel Stambrecht
 * /
 */

package cz.stambrecht.notes.io.rest.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Simple text note model class.
 *
 * @property id Id of note or null if no id defined.
 * @property title Title of note or null if no title available.
 *
 * @author Pavel Stambrecht
 * @date 18/03/2019
 */
@Parcelize
data class Note(
    var id: Long? = null,
    var title: String? = null
) : Parcelable {


    /**
     * Checks if note is removable
     * @return true if removable
     */
    fun isRemovable(): Boolean = id != null
}