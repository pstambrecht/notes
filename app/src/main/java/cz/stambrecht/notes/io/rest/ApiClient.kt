/*
 *  Copyright (C) 2019 Bc. Pavel Stambrecht - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 *  Written by Pavel Stambrecht
 * /
 */

package cz.stambrecht.notes.io.rest

import cz.stambrecht.notes.BuildConfig
import cz.stambrecht.notes.io.rest.api.ApiaryApi
import cz.stambrecht.notes.io.rest.interceptor.ApiaryHeaderInterceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * [ApiClient] represents client for communication with API.
 * Currently it uses [Retrofit] as a JSON Rest client.
 *
 * @author Pavel Stambrecht
 * @date 19/03/2019
 */
object ApiClient {

    /**
     * Apiary API
     */
    var apiaryApi: ApiaryApi = createClient().create(ApiaryApi::class.java)

    private fun createClient() = Retrofit.Builder()
        .baseUrl(BuildConfig.API_URL)
        .client(
            OkHttpClient.Builder()
                .addInterceptor(ApiaryHeaderInterceptor())
                .build()
        )
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}