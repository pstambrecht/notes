/*
 *  Copyright (C) 2019 Bc. Pavel Stambrecht - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 *  Written by Pavel Stambrecht
 * /
 */

package cz.stambrecht.notes.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import cz.stambrecht.notes.io.rest.ApiClient
import cz.stambrecht.notes.io.rest.exception.UnsuccessResponseException
import cz.stambrecht.notes.io.rest.model.Note
import cz.stambrecht.notes.utils.Asyncer
import java.io.IOException

/**
 * [NoteDetailViewModel] contains logic to serve and remove note.
 *
 * @see Note
 *
 * @author Pavel Stambrecht
 * @date 24/03/2019
 */
class NoteDetailViewModel(application: Application) : AndroidViewModel(application) {

    /**
     * Observable [Note]
     */
    val note: LiveData<Note>
        get() = noteField

    /**
     * Observable [Throwable] error.
     */
    val error: LiveData<Throwable>
        get() = errorThrowableField

    /**
     * Observable processing state
     */
    val isProcessing: LiveData<Boolean>
        get() = isProcessingField

    private val noteField = MutableLiveData<Note>()
    private val errorThrowableField = MutableLiveData<Throwable>()
    private val isProcessingField = MutableLiveData<Boolean>()

    /**
     * Initializes view model
     * @param note [Note] instance or null if no note available
     */
    fun init(note: Note?) {
        noteField.value = note
    }

    /**
     * Removes note on API
     */
    fun remove() {
        if (note.value?.isRemovable() == true) {
            isProcessingField.value = true
            Asyncer.process {
                val response = try {
                    ApiClient.apiaryApi.removeNote(note.value!!.id!!).execute()
                } catch (exception: IOException) {
                    isProcessingField.postValue(false)
                    errorThrowableField.postValue(exception)
                    return@process
                }

                if (response.isSuccessful) {
                    noteField.postValue(null)
                } else {
                    errorThrowableField.postValue(UnsuccessResponseException())
                }
                isProcessingField.postValue(false)
            }
        } else {
            errorThrowableField.postValue(IllegalStateException("Note does not have id"))
        }

    }
}