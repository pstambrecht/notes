/*
 *  Copyright (C) 2019 Bc. Pavel Stambrecht - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 *  Written by Pavel Stambrecht
 * /
 */

package cz.stambrecht.notes.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import cz.stambrecht.notes.io.rest.ApiClient
import cz.stambrecht.notes.io.rest.exception.UnsuccessResponseException
import cz.stambrecht.notes.io.rest.model.Note
import cz.stambrecht.notes.utils.Asyncer
import java.io.IOException
import java.util.*

/**
 * [NotesViewModel] is view model for screen with list of notes.
 * It provides operations such a load notes and create new note.
 *
 * @param application Application instance
 *
 * @author Pavel Stambrecht
 * @date 18/03/2019
 */
class NotesViewModel(application: Application) : AndroidViewModel(application) {

    /**
     * Observable list of notes
     * @see Note
     */
    val notes: LiveData<List<Note>>
        get() = notesField

    /**
     * Observable loading state. The state is true if something is loading/processing.
     */
    val isLoading: LiveData<Boolean>
        get() = isLoadingField

    /**
     * Observable [Throwable] error.
     */
    val error: LiveData<Throwable>
        get() = errorThrowableField

    private val notesField = MutableLiveData<List<Note>>()
    private val isLoadingField = MutableLiveData<Boolean>()
    private var loadingCount: Int = 0
    private val loadingLock = Any()
    private val errorThrowableField = MutableLiveData<Throwable>()

    private fun increaseLoadingCount() {
        synchronized(loadingLock) {
            loadingCount++
            if (loadingCount == 1) {
                isLoadingField.postValue(true)
            }
        }
    }

    private fun decreaseLoadingCount() {
        synchronized(loadingLock) {
            loadingCount--
            if (loadingCount < 1) {
                loadingCount = 0
                isLoadingField.postValue(false)
            }
        }
    }

    /**
     * Loads all notes from API. Result is posted to [notesField]
     */
    fun loadNotes() {
        increaseLoadingCount()
        Asyncer.process {
            val response = try {
                ApiClient.apiaryApi.getNotes().execute()
            } catch (exception: IOException) {
                errorThrowableField.postValue(exception)
                decreaseLoadingCount()
                return@process
            }

            if (response.isSuccessful) {
                notesField.postValue(response.body())
            } else {
                errorThrowableField.postValue(UnsuccessResponseException())
            }
            decreaseLoadingCount()
        }
    }

    /**
     * Creates new note on API. On success new note is added to currently loaded list of notes.
     * @param note Note to create (store) on API
     */
    fun createNote(note: Note) {
        increaseLoadingCount()
        Asyncer.process {
            val response = try {
                ApiClient.apiaryApi.createNote(note).execute()
            } catch (exception: IOException) {
                errorThrowableField.postValue(exception)
                decreaseLoadingCount()
                return@process
            }

            if (response.isSuccessful) {
                notesField.postValue(notesField.value?.let {
                    //if the note is successfully created on API
                    //then it means it is available in body
                    ArrayList<Note>(it).apply { add(response.body()!!) }
                } ?: ArrayList<Note>().apply { add(response.body()!!) })
            } else {
                errorThrowableField.postValue(UnsuccessResponseException())
            }
            decreaseLoadingCount()
        }
    }

}