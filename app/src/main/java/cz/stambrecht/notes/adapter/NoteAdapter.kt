/*
 *  Copyright (C) 2019 Bc. Pavel Stambrecht - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 *  Written by Pavel Stambrecht
 * /
 */

package cz.stambrecht.notes.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import cz.stambrecht.notes.R
import cz.stambrecht.notes.adapter.common.DiffAdapter
import cz.stambrecht.notes.adapter.viewholder.NoteViewHolder
import cz.stambrecht.notes.io.rest.model.Note

/**
 * [NoteAdapter] represents
 *
 * @author Pavel Stambrecht
 * @date 18/03/2019
 */
class NoteAdapter : DiffAdapter<Note, NoteViewHolder>(ItemCallback) {

    companion object {
        private val ItemCallback = object : DiffUtil.ItemCallback<Note>() {
            override fun areItemsTheSame(oldItem: Note, newItem: Note): Boolean = oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Note, newItem: Note): Boolean = oldItem.title == newItem.title
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder =
        NoteViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_note, parent, false))


    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.bind(getItem(position))
    }
}