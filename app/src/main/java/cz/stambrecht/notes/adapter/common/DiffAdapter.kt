/*
 *  Copyright (C) 2019 Bc. Pavel Stambrecht - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 *  Written by Pavel Stambrecht
 * /
 */

package cz.stambrecht.notes.adapter.common

import android.view.View
import androidx.annotation.CallSuper
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

/**
 * [DiffAdapter] is an abstract class extending [ListAdapter] to provide item click listener
 *
 * @author Pavel Stambrecht
 * @date 18/03/2019
 */
abstract class DiffAdapter<T, VH : RecyclerView.ViewHolder>(itemCallback: DiffUtil.ItemCallback<T>) :
    ListAdapter<T, VH>(itemCallback) {

    /**
     * [OnItemClickListener] instance to listen click events
     */
    @Suppress("unused")
    var onItemClickListener: OnItemClickListener<T>? = null

    /**
     * Common internal on click listener which determines clicked item by position.
     * It calls [onItemClickListener] with relevant data
     */
    private val mOnCommonItemClickListener = View.OnClickListener {
        val position: Int = it.tag as Int
        getItem(position)?.let { data ->
            onItemClickListener?.onItemClicked(data, getItemViewType(position))
        }
    }

    @CallSuper
    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.itemView.let {
            it.tag = position
            it.setOnClickListener(mOnCommonItemClickListener)
        }
    }

    @CallSuper
    override fun onViewRecycled(holder: VH) {
        super.onViewRecycled(holder)
        holder.itemView.setOnClickListener(null)
    }

    /**
     * [OnItemClickListener] listens to [T] item click
     */
    interface OnItemClickListener<T> {

        /**
         * Called while item clicked
         * @param item clicked item
         * @param type type of clicked item
         */
        fun onItemClicked(item: T, type: Int)
    }
}