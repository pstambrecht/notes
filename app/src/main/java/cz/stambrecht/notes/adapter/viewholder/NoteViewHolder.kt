/*
 *  Copyright (C) 2019 Bc. Pavel Stambrecht - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 *  Written by Pavel Stambrecht
 * /
 */

package cz.stambrecht.notes.adapter.viewholder

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cz.stambrecht.notes.R
import cz.stambrecht.notes.io.rest.model.Note

/**
 * [NoteViewHolder] represents [RecyclerView.ViewHolder] for Note view
 *
 * @param itemView Note item view. This view must contain TextView with textView_title id.
 *
 * @author Pavel Stambrecht
 * @date 18/03/2019
 */
class NoteViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    //Do not use kotlin extensions here. Classic findViewById has better recycling.
    private val titleTextView: TextView = itemView.findViewById(R.id.textView_title)
    private val noteBadgeTextView: TextView = itemView.findViewById(R.id.textView_noteBadge)

    /**
     * Binds [Note] data to item view
     * @param note Note instance to bind
     */
    fun bind(note: Note) {
        noteBadgeTextView.text = note.title?.first()?.toString() ?: "?"
        titleTextView.text = note.title
    }
}
