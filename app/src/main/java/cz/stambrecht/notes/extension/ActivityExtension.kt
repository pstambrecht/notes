/*
 *  Copyright (C) 2019 Bc. Pavel Stambrecht - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 *  Written by Pavel Stambrecht
 * /
 */

package cz.stambrecht.notes.extension

import android.app.Activity
import android.view.inputmethod.InputMethodManager

/**
 * Hides software keyboard.
 */
fun Activity.hideSoftwareKeyboard() {
    val imm = this.getSystemService(android.app.Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    var view = this.currentFocus
    if (view == null) {
        view = android.view.View(this)
    }
    imm.hideSoftInputFromWindow(view.windowToken, 0)
    view.clearFocus()
}