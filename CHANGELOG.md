# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Screen with list of note and capability to add new note.
- Bottom navigation.
- Screen with note detail and capability to remove note.
- Screen with information about application.
- Pull down to refresh gesture.
