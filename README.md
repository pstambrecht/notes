# Notes

Notes is a simple Android application written in Kotlin for managing notes.

## Build
Project uses Gradle to build. You can build it using Android studio or Gradle command below
```
./gradlew build
```

## TODO
- Capability to edit existing note.

## Known bugs
- Save button is not visible when creating new note and keyboard shows up.
